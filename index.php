<?php
require_once 'navbar.php'
?>
<!-- slider-start -->
<div class="slider-area">
    <div class="slider-active">
        <div class="single-slider slider-height-2  d-flex align-items-center" style="background-image:url(img/slider/slider2.jpg)">
            <div class="container">
                <div class="row ">
                    <div class="col-xl-12">
                        <div class="slider-content">
                            <h1 data-animation="fadeInUp" data-delay=".6s">Farm Spectrum East  <br> Africa Ltd</h1>
                            <p data-animation="fadeInUp" data-delay=".8s">Farm Spectrum East Africa Ltd is
                                a customer focused and responsive company dedicated to play a leading role in the
                                African agricultural advancement by pursuing new growth opportunities; </p>
                            <div class="slider-button">
                                <a data-animation="fadeInLeft" data-delay=".8s" class="btn" href="index-2.html#">Our Services</a>
                                <a data-animation="fadeInRight" data-delay="1s" class="btn active" href="index-2.html#">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="single-slider slider-height-2  d-flex align-items-center" style="background-image:url(img/slider/slider2-2.jpg)">
            <div class="container">
                <div class="row ">
                    <div class="col-xl-12">
                        <div class="slider-content">
                            <h1 data-animation="fadeInUp" data-delay=".6s">Integrate agricultural  <br> supply chain in EAC</h1>
                            <p data-animation="fadeInUp" data-delay=".8s">The company exists in the market place to enable local growers produce more and to enable buyers across Europe, America, Middle East, Australia and Asia
                                access only highest quality fresh produce, coffee and tea..</p>
                            <div class="slider-button">
                                <a data-animation="fadeInLeft" data-delay=".8s" class="btn" href="index-2.html#">Our Services</a>
                                <a data-animation="fadeInRight" data-delay="1s" class="btn active" href="index-2.html#">Contact us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- slider-start -->

<!-- we-do-start -->
<div class="we-do-area pt-110 pb-85">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 offset-lg-3 offset-xl-3">
                <div class="section-title text-center section-circle mb-70">
                    <div class="section-img">
                        <img src="img/shape/1.png" alt="" />
                    </div>
                    <h1>What We Do</h1>
                    <p>We are offering the benefits of
                        new innovative agro technologies and convenience of multiple products, brands and channels within and beyond the territorial limits of Kenya and the EAC trading bloc, to the benefit of our customers and our shareholders, elevating our image as a socially responsible and ethica
                        l company that is watched and emulated as a model of success.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="we-do-wrapper text-center mb-30">
                    <div class="we-do-img">
                        <img src="img/icon/icon1.png" alt="" />
                    </div>
                    <div class="we-do-text">
                        <h4><a href="index-2.html#">Natarul Food</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod te incididunt ut labore et dolore magna aliqua.</p>
                        <a href="index-2.html#">Read More <i class="dripicons-arrow-thin-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="we-do-wrapper text-center  mb-30">
                    <div class="we-do-img">
                        <img src="img/icon/icon2.png" alt="" />
                    </div>
                    <div class="we-do-text">
                        <h4><a href="index-2.html#">Biologically Safe</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod te incididunt ut labore et dolore magna aliqua.</p>
                        <a href="index-2.html#">Read More <i class="dripicons-arrow-thin-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="we-do-wrapper text-center mb-30">
                    <div class="we-do-img">
                        <img src="img/icon/icon3.png" alt="" />
                    </div>
                    <div class="we-do-text">
                        <h4><a href="index-2.html#">Conserve Biodiversity</a></h4>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod te incididunt ut labore et dolore magna aliqua.</p>
                        <a href="index-2.html#">Read More <i class="dripicons-arrow-thin-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- we-do-end -->

<!-- cta-area-start -->
<div class="cta-area pt-160 pb-160" style="background-image:url(img/bg/bg7.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="cta2-wrapper" style="background-image:url(img/bg/1.png)">
                    <div class="cta2-text text-center">
                        <h1>OUR MISSION <br> </h1>
                        <p>We are a market-and-customer-driven company committed to provision of high value and useful services to our customers.
                            The Farm Spectrum's mission is to provide the customer with the most satisfying procurement experience that they have ever experienced.</p>
                        <a class="btn" href="index-2.html#">View Other Services</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- cta-area-end -->

<!-- product-area-start -->
<div class="product-area pt-110 pb-85 pos-relative fix">
    <div class="shape spahe1 bounce-animate"><img src="img/shape/shape1.png" alt=""></div>
    <div class="shape spahe2 bounce-animate"><img src="img/shape/shape2.png" alt=""></div>
    <div class="shape spahe3 bounce-animate"><img src="img/shape/shape3.png" alt=""></div>
    <div class="shape spahe4 bounce-animate"><img src="img/shape/shape4.png" alt=""></div>
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 offset-lg-3 offset-xl-3">
                <div class="section-title text-center section-circle mb-70">
                    <div class="section-img">
                        <img src="img/shape/1.png" alt="" />
                    </div>
                    <h1>Our Export Products</h1>
<!--                    <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmotempor incididunt ut labore et dolore magna aliqua enim minim veniam</p>-->
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">


                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-xl-3 col-lg-3 col-md-6">
                                <div class="product-wrapper text-center mb-30">
                                    <div class="product2-img">
                                        <a href="index-2.html#"><img src="img/product/product1.jpg" alt="" /></a>
                                        <div class="product-action">
                                            <a href="index-2.html#"><i class="fas fa-shopping-cart"></i></a>
                                            <a href="index-2.html#"><i class="fas fa-heart"></i></a>
                                            <a href="index-2.html#"><i class="fas fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="product-text product2-text">
                                        <h4><a href="index-2.html#">Darling Oranges</a></h4>
                                        <div class="pro-rating">
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                        <div class="pro-price">
<!--                                            <span>$49.99</span>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6">
                                <div class="product-wrapper text-center mb-30">
                                    <div class="product2-img">
                                        <a href="index-2.html#"><img src="img/product/product2.jpg" alt="" /></a>
                                        <div class="product-action">
                                            <a href="index-2.html#"><i class="fas fa-shopping-cart"></i></a>
                                            <a href="index-2.html#"><i class="fas fa-heart"></i></a>
                                            <a href="index-2.html#"><i class="fas fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="product-text product2-text">
                                        <h4><a href="index-2.html#">Fresh Apples</a></h4>
                                        <div class="pro-rating">
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                        <div class="pro-price">
<!--                                            <span>$29.99</span>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6">
                                <div class="product-wrapper text-center mb-30">
                                    <div class="product2-img">
                                        <a href="index-2.html#"><img src="img/product/product3.jpg" alt="" /></a>
                                        <div class="product-action">
                                            <a href="index-2.html#"><i class="fas fa-shopping-cart"></i></a>
                                            <a href="index-2.html#"><i class="fas fa-heart"></i></a>
                                            <a href="index-2.html#"><i class="fas fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="product-text product2-text">
                                        <h4><a href="index-2.html#">Natural Broccoli</a></h4>
                                        <div class="pro-rating">
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                        <div class="pro-price">
<!--                                            <span>$25.99</span>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-3 col-lg-3 col-md-6">
                                <div class="product-wrapper text-center mb-30">
                                    <div class="product2-img">
                                        <a href="index-2.html#"><img src="img/product/product4.jpg" alt="" /></a>
                                        <div class="product-action">
                                            <a href="index-2.html#"><i class="fas fa-shopping-cart"></i></a>
                                            <a href="index-2.html#"><i class="fas fa-heart"></i></a>
                                            <a href="index-2.html#"><i class="fas fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="product-text product2-text">
                                        <h4><a href="index-2.html#">Seasoned Tomatoes</a></h4>
                                        <div class="pro-rating">
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                            <i class="far fa-star"></i>
                                        </div>
                                        <div class="pro-price">
<!--                                            <span>$49.99</span>-->
                                        </div>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- product-area-end -->

<!-- choose-us-area-start -->
<div class="choose-us-area pt-120 pb-90" style="background-image:url(img/bg/bg8.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="choose2-img mb-30">
                    <img src="img/choose/02.png" alt="" />
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 col-md-6">
                <div class="single-choose mb-30">
                    <div class="choose-title">
                        <h1>Our Vision </h1>
                        <p>Our vision is for Farm Spectrum E.A Ltd to be the best exporter of fresh farm produce green coffee beans and black tea within the EAC and COMESA markets.</p>
                    </div>
                    <div class="choose2-content">
                        <div class="choose2-icon-img">
                            <img src="img/icon/4.png" alt="" />
                        </div>
                        <div class="choose2-text">
                            <p>Gaining a full participatory framework for non-executive Directors.</p>
                        </div>
                    </div>
                    <div class="choose2-content">
                        <div class="choose2-icon-img">
                            <img src="img/icon/4.png" alt="" />
                        </div>
                        <div class="choose2-text">
                            <p>Engaging stakeholders more strategically to advance organizational objectives..</p>
                        </div>
                    </div>
                    <div class="choose2-content">
                        <div class="choose2-icon-img">
                            <img src="img/icon/4.png" alt="" />
                        </div>
                        <div class="choose2-text">
                            <p>Positioning the organization to always remain contemporary in strategic planning.</p>
                        </div>
                    </div>
                    <div class="choose2-content">
                        <div class="choose2-icon-img">
                            <img src="img/icon/4.png" alt="" />
                        </div>
                        <div class="choose2-text">
                            <p>Increasing the level of accountability at board level for improved performance and ROI..</p>
                        </div>
                    </div>
                    <div class="choose-button">
                        <a class="btn" href="about.php">Learn More</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- choose-us-area-end -->









<?php
require_once 'footer.php'
?>
