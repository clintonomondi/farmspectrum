<!-- footer-area-start -->
<div class="footer-area pt-200" style="background-image:url(img/bg/bg6.jpg)">
    <div class="container">
        <div class="footer-bg pb-50">
            <div class="row">
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="footer-wrapper mb-30">
                        <div class="footer-title">
                            <h4>Farm Spectrum EA Ltd</h4>
                            <img src="img/shape/f.png" alt="" />
                        </div>
                        <div class="footer-text">
                            <p>The inspiration and driving force behind Farm Spectrum East Africa Ltd is to enable the farming community within East Africa markets achieve its full potential. This is facilitated by creation of cost effective supply networks for farm-in-puts especially to harness and develop the capacity of farmers growing vegetables, fruits, coffee and tea..</p>
                        </div>
                        <div class="footer-icon">
                            <a href="about.html#"><i class="fab fa-facebook-f"></i></a>
                            <a href="about.html#"><i class="fab fa-twitter"></i></a>
                            <a href="about.html#"><i class="fab fa-linkedin"></i></a>
                            <a href="about.html#"><i class="fab fa-youtube"></i></a>
                            <a href="about.html#"><i class="fab fa-behance"></i></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="footer-wrapper pl-45 mb-30">
                        <div class="footer-title">
                            <h4>Quick Links</h4>
                            <img src="img/shape/f.png" alt="" />
                        </div>
                        <ul class="fotter-menu">
                            <li><a href="index.php">Home</a></li>
                            <li><a href="about.php">About us</a></li>
                            <li><a href="service.php">Services</a></li>
                            <li><a href="contact.php">Contact us</a></li>
                            <li><a href="about.html#">Get a Call</a></li>
                            <li><a href="about.html#">Online Enquiry</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="footer-wrapper pl-45 mb-30">
                        <div class="footer-title">
                            <h4>Contact Info</h4>
                            <img src="img/shape/f.png" alt="" />
                        </div>
                        <ul class="fotter-link">
                            <li><i class="far fa-paper-plane"></i>The offices of the company are located at the Spectrum Centre, Rangwe Road, Off Lunga Lunga Road, Industrial Area, Nairobi.</li>
                            <li><i class="far fa-envelope-open"></i> info@farmspectrum.co.ke;
                            </li>
                            <li><i class="fas fa-headphones"></i>info@farmspectrum.co.ke;
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-3 col-lg-3 col-md-6">
                    <div class="footer-wrapper pl-40 mb-30">
                        <div class="footer-title">
                            <h4>Newsletters</h4>
                            <img src="img/shape/f.png" alt="" />
                        </div>
                        <div class="footer-content">
                            <p>Enter your email and we’ll send you latest information plans.</p>
                        </div>
                        <div class="subscribes-form">
                            <form action="about.html#">
                                <input placeholder="Enter your emaill " type="email">
                                <button class="btn">Subscribe</button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom-area">
            <div class="row">
                <div class="col-xl-12">
                    <div class="copyright text-center">
                        <p>Copyright <i class="far fa-copyright"></i> 2021 <a href="index.php">Farm Spectrum East Africa Ltd</a>. All Rights Reserved</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- footer-area-end -->
















<!-- Modal Search -->
<div class="modal fade" id="search-modal" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form>
                <input type="text" placeholder="Search here...">
                <button>
                    <i class="fa fa-search"></i>
                </button>
            </form>
        </div>
    </div>
</div>











<!-- JS here -->
<script src="js/vendor/modernizr-3.5.0.min.js"></script>
<script src="js/vendor/jquery-1.12.4.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/waypoints.min.js"></script>
<script src="js/jquery.counterup.min.js"></script>
<script src="js/jquery.countdown.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/isotope.pkgd.min.js"></script>
<script src="js/jquery.meanmenu.min.js"></script>
<script src="js/slick.min.js"></script>
<script src="js/ajax-form.js"></script>
<script src="js/wow.min.js"></script>
<script src="js/jquery.scrollUp.min.js"></script>
<script src="js/imagesloaded.pkgd.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/plugins.js"></script>
<script src="js/main.js"></script>
</body>
</html>
