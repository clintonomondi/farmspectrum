<!doctype html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Farm Spectrum East Africa Ltd  </title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="manifest" href="https://www.devsnews.com/template/zomata/zomata/site.webmanifest">
    <link rel="shortcut icon" type="image/x-icon" href="img/logo/logo2.png">
    <!-- Place favicon.png in the root directory -->

    <!-- CSS here -->
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="css/owl.carousel.min.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/meanmenu.css">
    <link rel="stylesheet" href="css/flaticon.css">
    <link rel="stylesheet" href="css/fontawesome-all.min.css">
    <link rel="stylesheet" href="css/slick.css">
    <link rel="stylesheet" href="css/dripicons.css">
    <link rel="stylesheet" href="css/themify-icons.css">
    <link rel="stylesheet" href="css/default.css">
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/responsive.css">
</head>
<body>



<!-- header-start -->
<header>
    <div class="header-area header-2 d-none d-md-block">
        <div class="container">
            <div class="row">
                <div class="col-xl-7 col-lg-7 col-md-9">
                    <div class="header-wrapper">
                        <div class="header-text">
                            <span> <i class="far fa-map"></i> Box 52113-00100, Nairobi, Kenya. Telephone: +254701664056</span>
                            <span> <i class="far fa-envelope"></i> info@farmspectrum.co.ke;</span>
                        </div>
                    </div>
                </div>
                <div class="col-xl-5 col-lg-5 col-md-3">
                    <div class="header-icon text-md-right">
                        <a href="index-2.html#"><i class="fab fa-facebook-f"></i></a>
                        <a href="index-2.html#"><i class="fab fa-twitter"></i></a>
                        <a href="index-2.html#"><i class="fab fa-linkedin"></i></a>
                        <a href="index-2.html#"><i class="fab fa-youtube"></i></a>
                        <a href="index-2.html#"><i class="fab fa-behance"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="sticky-header" class="main-menu-area">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-xl-4 col-lg-4 col-md-4 col-6">
                    <div class="logo d-flex">
                        <a href="/" class="">
<!--                            <img class="standard-logo" src="img/logo/east.png" alt="" />-->
                            <img class="retina-logo" src="img/logo/logo2.png" alt="" />
                        </a>
                        <h4>Farm Spectrum</h4>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8 col-md-8 col-6">
                    <div class="header-right f-right">
                        <ul>
                            <li class="search-icon"><a href="#" data-toggle="modal" data-target="#search-modal"><i class="dripicons-search"></i></a></li>
                            <li class="active info-bar"><a href="#"><i class="fa fa-envelope"></i></a></li>
                        </ul>
                    </div>
                    <div class="main-menu text-right f-right">
                        <nav id="mobile-menu">
                            <ul>
                                <li <?php if($_SERVER['SCRIPT_NAME']=="/") { ?>  class="active"   <?php   } else{ ?>class="" <?php }?>><a href="index.php">home </a>
                                </li>
                                <li><a href="about.php" <?php if($_SERVER['SCRIPT_NAME']=="/about.php") { ?>  class="active"   <?php   } else{ ?>class="" <?php }?>>about</a></li>
                                <li><a href="service.php" <?php if($_SERVER['SCRIPT_NAME']=="/service.php") { ?>  class="active"   <?php   } else{ ?>class="" <?php }?>>Services</a></li>
                                <li><a href="contact.php" <?php if($_SERVER['SCRIPT_NAME']=="/contact.php") { ?>  class="active"   <?php   } else{ ?>class="" <?php }?>>Contact</a></li>
                            </ul>
                        </nav>
                    </div>
                </div>
                <div class="col-12">
                    <div class="mobile-menu"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="extra-info">
        <div class="close-icon">
            <button>
                <i class="far fa-window-close"></i>
            </button>
        </div>
        <div class="logo-side mb-30">
            <a href="index.php">
                <img src="img/logo/logo2.png" alt="" />
            </a>
        </div>
        <div class="side-info mb-30">
            <div class="contact-list mb-30">
                <h4>Office Address</h4>
                <p>Box 52113-00100, Nairobi, Kenya. Telephone: +254701664056</p>
            </div>
            <div class="contact-list mb-30">
                <h4>Phone Number</h4>
                <p>+254701664056</p>
            </div>
            <div class="contact-list mb-30">
                <h4>Email Address</h4>
                <p>info@farmspectrum.co.ke;
                </p>
            </div>
        </div>
        <div class="instagram">
            <a href="index-2.html#">
                <img src="img/gallery/gallery1.jpg" alt="">
            </a>
            <a href="index-2.html#">
                <img src="img/gallery/gallery2.jpg" alt="">
            </a>
            <a href="index-2.html#">
                <img src="img/gallery/gallery3.jpg" alt="">
            </a>
            <a href="index-2.html#">
                <img src="img/gallery/gallery4.jpg" alt="">
            </a>
            <a href="index-2.html#">
                <img src="img/gallery/gallery5.jpg" alt="">
            </a>
            <a href="index-2.html#">
                <img src="img/gallery/gallery6.jpg" alt="">
            </a>
        </div>
        <div class="social-icon-right mt-20">
            <a href="index-2.html#">
                <i class="fab fa-facebook-f"></i>
            </a>
            <a href="index-2.html#">
                <i class="fab fa-twitter"></i>
            </a>
            <a href="index-2.html#">
                <i class="fab fa-google-plus-g"></i>
            </a>
            <a href="index-2.html#">
                <i class="fab fa-instagram"></i>
            </a>
        </div>
    </div>
</header>
<!-- header-end -->