<?php
require_once 'navbar.php'
?>
<!-- breadcrumb-area-start -->
<div class="breadcrumb-area pt-160 pb-170" style="background-image:url(img/bg/bg15.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-text text-center">
                    <h1>contact</h1>
                    <ul class="breadcrumb-menu">
                        <li><a href="index.html">home</a></li>
                        <li><span>contact</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>




<!-- contact-area-start -->
<div class="contact-area">
    <div class="container">
        <div class="contact-bg pt-90 pb-70" style="background-image:url(img/bg/bg18.jpg)">
            <div class="row">
                <div class="col-xl-12">
                    <div class="contact-title text-center mb-35">
                        <h1>Leave a Message</h1>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-xl-12">
                    <div class="contact-wrapper mb-30">
                        <form action="https://www.devsnews.com/template/zomata/zomata/mail.php" id="contact-us-form">
                            <div class="row">
                                <div class="col-xl-6 col-lg-6">
                                    <input name="name" placeholder="Name :" type="text">
                                </div>
                                <div class="col-xl-6 col-lg-6">
                                    <input name="email" placeholder="Email :" type="email">
                                </div>
                                <div class="col-xl-12 col-lg-12">
                                    <input name="website" placeholder="Website :" type="text">
                                </div>
                                <div class="col-xl-12 col-lg-12">
                                    <textarea name="message" cols="30" rows="10" placeholder="Write your comments :"></textarea>
                                </div>
                                <div class="col-xl-12 col-lg-12">
                                    <div class="contact-button text-center">
                                        <button class="btn" type="submit">Send Message</button>
                                    </div>
                                </div>
                            </div>
                            <p class="ajax-response"></p>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- contact-area-end -->

<?php
require_once 'footer.php'
?>
