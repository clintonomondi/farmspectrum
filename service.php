<?php
require_once 'navbar.php'
?>

<!-- breadcrumb-area-start -->
<div class="breadcrumb-area pt-160 pb-170" style="background-image:url(img/bg/bg15.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-text text-center">
                    <h1>Our Services</h1>
                    <ul class="breadcrumb-menu">
                        <li><a href="index.html">home</a></li>
                        <li><span>Our Services</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area-end -->

<!-- our-service-area-start -->
<div class="our-service-area pt-120 pb-45">
    <div class="container">
        <div class="row">
            <div class="col-xl-5 col-lg-5 col-md-5">
                <div class="our-service-img mb-30">
                    <img src="img/service/1.png" alt="" />
                </div>
            </div>
            <div class="col-xl-6 col-lg-7 col-md-7">
                <div class="our-service-text mb-30">
                    <h1>Years of Industry Experience</h1>
                    <p> Farm Spectrum operates by representing selected farming cooperatives and local communities, with the help of mandates who possess full legal and corporate responsibility and who act on their behalf to facilitate and manage operations..</p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- our-service-area-end -->

<!-- services-area-start -->
<div class="services-area pb-60">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="services-wrapper single-services mb-60">
                    <div class="services-img">
                        <a href="services.html#"><img src="img/service/4.jpg" alt="" /></a>
                    </div>
                    <div class="services-text text-center">
                        <h3><a href="services.html#">Organic Vegetables</a></h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod te incididunt ut labore et dolore magna aliqua.</p>
                        <a href="services.html#">Read More <i class="dripicons-arrow-thin-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="services-wrapper single-services mb-60">
                    <div class="services-img">
                        <a href="services.html#"><img src="img/service/5.jpg" alt="" /></a>
                    </div>
                    <div class="services-text text-center">
                        <h3><a href="services.html#">Organic Vegetables</a></h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod te incididunt ut labore et dolore magna aliqua.</p>
                        <a href="services.html#">Read More <i class="dripicons-arrow-thin-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="services-wrapper single-services mb-60">
                    <div class="services-img">
                        <a href="services.html#"><img src="img/service/3.jpg" alt="" /></a>
                    </div>
                    <div class="services-text text-center">
                        <h3><a href="services.html#">Organic Vegetables</a></h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod te incididunt ut labore et dolore magna aliqua.</p>
                        <a href="services.html#">Read More <i class="dripicons-arrow-thin-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="services-wrapper single-services mb-60">
                    <div class="services-img">
                        <a href="services.html#"><img src="img/service/6.jpg" alt="" /></a>
                    </div>
                    <div class="services-text text-center">
                        <h3><a href="services.html#">Organic Vegetables</a></h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod te incididunt ut labore et dolore magna aliqua.</p>
                        <a href="services.html#">Read More <i class="dripicons-arrow-thin-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="services-wrapper single-services mb-60">
                    <div class="services-img">
                        <a href="services.html#"><img src="img/service/7.jpg" alt="" /></a>
                    </div>
                    <div class="services-text text-center">
                        <h3><a href="services.html#">Organic Vegetables</a></h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod te incididunt ut labore et dolore magna aliqua.</p>
                        <a href="services.html#">Read More <i class="dripicons-arrow-thin-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4 col-md-6">
                <div class="services-wrapper single-services mb-60">
                    <div class="services-img">
                        <a href="services.html#"><img src="img/service/8.jpg" alt="" /></a>
                    </div>
                    <div class="services-text text-center">
                        <h3><a href="services.html#">Organic Vegetables</a></h3>
                        <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmod te incididunt ut labore et dolore magna aliqua.</p>
                        <a href="services.html#">Read More <i class="dripicons-arrow-thin-right"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- service-area-end -->



<!-- zomata-area-start -->
<div class="zomata-area pt-120 pb-80">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6 cl-md-6">
                <div class="zomata-img mb-30">
                    <img src="img/bg/4.png" alt="" />
                </div>
            </div>
            <div class="col-xl-6 col-lg-6 cl-md-6">
                <div class="zomata-wrapper mb-30">
                    <div class="zomata-text">
                        <h1>WHY WORK WITH  <br> FARM SPECTRUM</h1>
                        <p>The inspiration and driving force behind Farm Spectrum East Africa Ltd is to enable the farming community within East Africa markets achieve its full potential. This is facilitated by creation of cost effective supply networks for farm-in-puts especially to harness and develop the capacity of farmers growing vegetables, fruits, coffee and tea. </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- zomata-area-end -->





<?php
require_once 'footer.php'
?>
