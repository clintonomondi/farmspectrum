<?php
require_once 'navbar.php'
?>


<!-- breadcrumb-area-start -->
<div class="breadcrumb-area pt-160 pb-170" style="background-image:url(img/bg/bg15.jpg)">
    <div class="container">
        <div class="row">
            <div class="col-xl-12">
                <div class="breadcrumb-text text-center">
                    <h1>About Us</h1>
                    <ul class="breadcrumb-menu">
                        <li><a href="index.php">home</a></li>
                        <li><span>About Us</span></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area-end -->

<!-- about-us-area-start -->
<div class="about-us-area about-shape pt-120 pb-90">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 col-lg-6">
                <div class="about-info mb-30">
                    <h1>About <br>Farm Spectrum East Africa Ltd </h1>
                    <span>Mitigating and eliminating corruption through transparent reporting and communication structures.</span>
                <p>Farm Spectrum East Africa Ltd is a customer focused and responsive company dedicated
                    to play a leading role in the African agricultural advancement by pursuing new growth
                    opportunities; offering the benefits of new innovative agro technologies and convenience
                    of multiple products, brands and channels within and beyond the territorial limits of Kenya
                    and the EAC trading bloc, to the benefit of our customers and our shareholders, elevating our
                    image as a socially responsible and ethical company that is watched and emulated as a model of success.</p>
                </div>
            </div>
            <div class="col-xl-6 col-lg-6">
                <div class="about-img mb-30">
                    <img src="img/about/2.jpg" alt="" />
                </div>
            </div>
        </div>
    </div>
</div>
<!-- about-us-area-end -->

<!-- faq-area-start -->
<div class="faq-area gray2-bg pt-105 pb-100">
    <div class="faq-img d-none d-md-block" style="background-image:url(img/bg/bg13.jpg)"></div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-xl-6 offset-xl-6 col-lg-6 offset-lg-6 col-md-6 offset-md-6">
                <div class="question-collapse">
                    <div class="faq-title">
                        <h1 style="text-align: center;">	OUR VISION <br> </h1>
                    </div>
                    <div id="accordion">
                        <div class="card">
                            <div class="card-header" id="headingOne">
                                <h5 class="mb-0">
                                    <a class="btn-link" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                      
                                    </a>
                                </h5>
                            </div>

                            <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Our vision is for Farm Spectrum E.A Ltd to be the best exporter of fresh farm produce green coffee beans and black tea within the EAC and COMESA markets.</p><br><br>

                                    <h1>This means</h1>
<ul>
  <li>Gaining a full participatory framework for non-executive Directors.</li>
  <li>Engaging stakeholders more strategically to advance organizational objectives.</li>
  <li>Positioning the organization to always remain contemporary in strategic planning.</li>
  <li>Increasing the level of accountability at board level for improved performance and ROI.</li>
  <li>Mitigating and eliminating corruption through transparent reporting and communication structures.</li>
</ul>
                                </div>
                            </div>
                        </div>
                       
                        <div class="card">
                            <div class="card-header" id="headingTwo">
                            <h1 style="text-align: center;">CORE  VALUES</h1>
                               
                                <ol>
  <li>i.	Respect</li>
  <li>ii.	Co-operation iii Accountability</li>
  <li>iv.	Continuous improvement</li>
  <li>v.	Courteous and quality service</li>
</ol>
                            </div>
                            
                            <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat aute irure aliquam quaerat.</p>
                                </div>
                            </div>
                        </div>
                        <div class="faq-title">
                        <h1 style="text-align: center;">	OUR MISSION <br> </h1>
                    </div>
                        <div class="card">
                            <div class="card-header" id="headingThree">
                              <ul style="margin-left: 35px;">
                                  <li>We are a market-and-customer-driven company committed to provision of high value and useful services to our customers.</li>
                                  <li>The Farm Spectrum's mission is to provide the customer with the most satisfying procurement experience that they have ever experienced</li>
                              </ul>
                            </div>
                            <div id="collapseThree" class="collapse" aria-labelledby="headingThree" data-parent="#accordion">
                                <div class="card-body">
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip exea commodo consequat aute irure aliquam quaerat.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- faq-area-end -->



<

<!-- testimonial-area-start -->
<div class="testimonial-area pt-110 pb-175">
    <div class="container">
        <div class="col-xl-6 col-lg-6 offset-lg-3 offset-xl-3">
            <div class="section-title section-circle text-center mb-70">
                <div class="section-img">
                    <img src="img/shape/1.png" alt="" />
                </div>
                <h1>Clients Reviews</h1>
                <p>Lorem ipsum dolor sit amet consectetur adipisicing elit sed do eiusmotempor incididunt ut labore et dolore magna aliqua enim minim veniam</p>
            </div>
        </div>
        <div class="testimonial-bg">
            <div class="row">
                <div class="testimonial2-active owl-carousel">
                    <div class="col-xl-12">
                        <div class="testimonial2-wrapper mb-30">
                            <div class="testimonial-text">
                                <p>Lorem ipsum dolor consectet elit sed do eiustemp orcididunt ut labore ethers dolore magna aliqua. Ut enim minimveniam feelsquis nostrud exercitation ullamco laboris.</p>
                                <div class="testimonial-content">
                                    <div class="testimonial2-img">
                                        <img src="img/testimonial/3.png" alt="" />
                                    </div>
                                    <div class="testimonial-name">
                                        <h4>Sileen P. Willilams</h4>
                                        <span>Web Designer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="testimonial2-wrapper mb-30">
                            <div class="testimonial-text">
                                <p>Lorem ipsum dolor consectet elit sed do eiustemp orcididunt ut labore ethers dolore magna aliqua. Ut enim minimveniam feelsquis nostrud exercitation ullamco laboris.</p>
                                <div class="testimonial-content">
                                    <div class="testimonial2-img">
                                        <img src="img/testimonial/4.png" alt="" />
                                    </div>
                                    <div class="testimonial-name">
                                        <h4>Sileen P. Willilams</h4>
                                        <span>Web Designer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="testimonial2-wrapper mb-30">
                            <div class="testimonial-text">
                                <p>Lorem ipsum dolor consectet elit sed do eiustemp orcididunt ut labore ethers dolore magna aliqua. Ut enim minimveniam feelsquis nostrud exercitation ullamco laboris.</p>
                                <div class="testimonial-content">
                                    <div class="testimonial2-img">
                                        <img src="img/testimonial/3.png" alt="" />
                                    </div>
                                    <div class="testimonial-name">
                                        <h4>Sileen P. Willilams</h4>
                                        <span>Web Designer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="testimonial2-wrapper mb-30">
                            <div class="testimonial-text">
                                <p>Lorem ipsum dolor consectet elit sed do eiustemp orcididunt ut labore ethers dolore magna aliqua. Ut enim minimveniam feelsquis nostrud exercitation ullamco laboris.</p>
                                <div class="testimonial-content">
                                    <div class="testimonial2-img">
                                        <img src="img/testimonial/4.png" alt="" />
                                    </div>
                                    <div class="testimonial-name">
                                        <h4>Sileen P. Willilams</h4>
                                        <span>Web Designer</span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- testimonial-area-end -->



<?php
require_once 'footer.php'
?>